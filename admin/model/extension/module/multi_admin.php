<?php
    class ModelExtensionModuleMultiAdmin extends Model {

        public function install(){
            $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "user_group_store` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `user_id` int(11) NOT NULL,
                `user_group_id` int(11) NOT NULL,
                `store_id` int(11) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

            // $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "role` (
            //     `role_id` int(11) NOT NULL AUTO_INCREMENT,
            //     `role_name` varchar(100) NOT NULL,
            //     PRIMARY KEY (`role_id`)
            // ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

            // $this->db->query("INSERT INTO `" . DB_PREFIX . "role` (role_name) VALUES ('Super Admin'), ('Store Admin')");
        }

        public function uninstall(){
            $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "user_group_store`");
            // $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "role`");
        }

        public function addMultiAdmin($data){
            $this->db->query("INSERT INTO `" . DB_PREFIX . "user_group_store` SET user_id = '" . $this->db->escape($data['user_id']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', store_id = '" . (int)$data['store_id'] . "'");
        
            return $this->db->getLastId();
        }

        public function editMultiAdmin($id, $data){
            $this->db->query("UPDATE `" . DB_PREFIX . "user_group_store` SET user_id = '" . (int)$data['user_id'] . "', user_group_id = '" . (int)$data['user_group_id'] . "', store_id = '" . (int)$data['store_id'] . "' WHERE id = '" . (int)$id . "'");
    
            // if ($data['password']) {
            //     $this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE user_id = '" . (int)$user_id . "'");
            // }
        }

        public function deleteMultiAdmin($id){
		    $this->db->query("DELETE FROM `" . DB_PREFIX . "user_group_store` WHERE id = '" . (int)$id . "'");
        }

        public function getMultiAdmin($id){
            
            // $sql = "SELECT a.username, a.user_id, b.role_name, b.role_id, c.name, c.store_id, d.id FROM " . DB_PREFIX . "user a JOIN " . DB_PREFIX . "user_role_store d ON (a.user_id = d.user_id) JOIN " . DB_PREFIX . "role b ON (b.role_id = d.role_id) JOIN " . DB_PREFIX . "store c ON (c.store_id = d.store_id) WHERE d.id = ".$id;
    
            // $query = $this->db->query($sql);
    
            // return $query->row;

            $sql = "SELECT a.username, a.user_id, b.name as user_group_name, b.user_group_id, c.name as store_name, c.store_id, d.id FROM " . DB_PREFIX . "user a JOIN " . DB_PREFIX . "user_group_store d ON (a.user_id = d.user_id) JOIN " . DB_PREFIX . "user_group b ON (b.user_group_id = d.user_group_id) JOIN " . DB_PREFIX . "store c ON (c.store_id = d.store_id) WHERE d.id = ".$id;
    
            $query = $this->db->query($sql);
    
            return $query->row;
        }

        public function getMultiAdmins($data = array()){
            
            $sql = "SELECT a.username, b.name as user_group_name, c.name as store_name, d.id FROM " . DB_PREFIX . "user a JOIN " . DB_PREFIX . "user_group_store d ON (a.user_id = d.user_id) JOIN " . DB_PREFIX . "user_group b ON (b.user_group_id = d.user_group_id) JOIN " . DB_PREFIX . "store c ON (c.store_id = d.store_id)";
    
            // $sort_data = array(
            //     'username',
            //     'status',
            //     'date_added'
            // );
    
            // if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            //     $sql .= " ORDER BY " . $data['sort'];
            // } else {
            //     $sql .= " ORDER BY username";
            // }
    
            // if (isset($data['order']) && ($data['order'] == 'DESC')) {
            //     $sql .= " DESC";
            // } else {
            //     $sql .= " ASC";
            // }
    
            // if (isset($data['start']) || isset($data['limit'])) {
            //     if ($data['start'] < 0) {
            //         $data['start'] = 0;
            //     }
    
            //     if ($data['limit'] < 1) {
            //         $data['limit'] = 20;
            //     }
    
            //     $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            // }
    
            $query = $this->db->query($sql);
    
            return $query->rows;
        }

        public function getTotalMultiAdmin() {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user_group_store`");
    
            return $query->row['total'];
        }
    
        public function getMultiAdminByUseridStoreid($user_id, $store_id){
            $sql = "SELECT * FROM `" . DB_PREFIX . "user_group_store` WHERE user_id = '".$user_id."' AND store_id = '".$store_id."'";
            $query = $this->db->query($sql);

            return $query->row;
        }

        public function getRoles($data = array()) {
            $sql = "SELECT * FROM `" . DB_PREFIX . "role`";
            $query = $this->db->query($sql);

            return $query->rows;

        }
    }