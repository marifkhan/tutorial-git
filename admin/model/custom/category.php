<?php
// catalog/model/custom/product.php
class ModelCustomCategory extends Model {
  function getProductLastModifiedInfo($data = array()) {

    if ($data['store_id'] > 0) {
		$sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) JOIN " . DB_PREFIX . "category_to_store cts ON (cts.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cts.store_id = '".(int)$data['store_id']."'";
        
        // $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.levels SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) LEFT JOIN oc_category_to_store cts ON (cd2.category_id = cts.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cts.store_id= '".$data['store_id']."'";
    }
    else{
		$sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";
    }

    if (!empty($data[0]['filter_name'])) {
        $sql .= " AND cd2.name LIKE '%" . $this->db->escape($data[0]['filter_name']) . "%'";
    }

    $sql .= " GROUP BY cp.category_id";

    $sort_data = array(
        'name',
        'sort_order'
    );

    if (isset($data[0]['sort']) && in_array($data[0]['sort'], $sort_data)) {
        $sql .= " ORDER BY " . $data[0]['sort'];
    } else {
        $sql .= " ORDER BY sort_order";
    }

    if (isset($data[0]['order']) && ($data[0]['order'] == 'DESC')) {
        $sql .= " DESC";
    } else {
        $sql .= " ASC";
    }

    if (isset($data[0]['start']) || isset($data[0]['limit'])) {
        if ($data[0]['start'] < 0) {
            $data[0]['start'] = 0;
        }

        if ($data[0]['limit'] < 1) {
            $data[0]['limit'] = 20;
        }

        $sql .= " LIMIT " . (int)$data[0]['start'] . "," . (int)$data[0]['limit'];
    }

    $query = $this->db->query($sql);

    // echo '<pre>';
    // print_r($query->rows);die;

    return $query->rows;
  }
  
  public function getTotalCategories($store_id = '') {
      if($store_id > 0){
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c JOIN oc_category_to_store cs ON (c.category_id = cs.category_id) WHERE cs.store_id = '".(int)$store_id."'");
      }
      else{
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category");
    }
    
    return $query->row['total'];
    }
}