<?php

    // Heading
    $_['heading_title']          = 'Multi Admin';

    $_['text_multi_admin']  = 'Multi Admin';
    
    $_['text_extension']      = 'Extensions';
    //$_['text_success']     = 'Success: You have modified "Multi Admin" module!';
    // $_['text_edit']        = 'Edit "Multi Admin" module';
    
    // Entry
    // $_['entry_status']     = 'Status';
    
    // Error
    // $_['error_permission'] = 'Warning: You do not have permission to modify "Multi Admin" module!';

    // Text
    $_['text_success']          = 'Success: You have modified store admin!';
    $_['text_list']             = 'Multi Admin List';
    $_['text_add']              = 'Add Admin';
    $_['text_edit']             = 'Edit Admin';

    // Column
    $_['column_username']       = 'Username';
    // $_['column_status']         = 'Status';
    // $_['column_date_added']     = 'Date Added';
    $_['column_role_name']      = 'Role';
    $_['column_store_name']     = 'Store';
    $_['column_action']         = 'Action';

    // Entry
    $_['entry_username']        = 'Username';
    $_['entry_role_name']       = 'Role';
    $_['entry_store_name']      = 'Store';

    // Error
    $_['error_permission']      = 'Warning: You do not have permission to modify users!';

    $_['error_username']        = 'Select user!';
    $_['error_role_name']       = 'Select role of user!';
    $_['error_store_name']      = 'Select store!';
    $_['error_exist_assign_role'] = 'Warning: User is already assign to this store!';

