<?php
    /**
     * Webkul Software.
     *
     * @category Webkul
     * @package Opencart Module Tutorial
     * @author Webkul
     * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
     * @license https://store.webkul.com/license.html
     */
    // Heading
    $_['heading_title']    = 'Hello World Module';
    
    $_['text_module']      = 'Modules';
    $_['text_success']     = 'Success: You have modified "Hello World Module" module!';
    $_['text_edit']        = 'Edit "Hello World Module" Module';
    
    // Entry
    $_['entry_status']     = 'Status';
    
    // Error
    $_['error_permission'] = 'Warning: You do not have permission to modify "Hello World Module" module!';
