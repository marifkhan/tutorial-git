<?php
    // admin/controller/module/demoevent.php
    class ControllerExtensionModuleMultiAdmin extends Controller {
    /**
     * property named $error is defined to put errors
     * @var array
     */
    private $error = array();
    
    public function install() {
		$this->load->model('setting/event');
		$this->load->model('extension/module/multi_admin');
		$this->model_extension_module_multi_admin->install();
		
        $this->model_setting_event->addEvent('event_category_info', 'admin/model/catalog/category/getCategories/before', 'custom/category/info');
        $this->model_setting_event->addEvent('event_category_total_count', 'admin/model/catalog/category/getTotalCategories/before', 'custom/category/totalcountcategory');
        $this->model_setting_event->addEvent('event_column_left_modification', 'admin/view/common/column_left/before', 'custom/category/columnleftmodification');
        $this->model_setting_event->addEvent('event_login_after', 'admin/controller/common/login/before', 'custom/category/loginafter');
        $this->model_setting_event->addEvent('event_logout_before', 'admin/controller/common/logout/before', 'custom/category/logoutbefore');        
    }
     
    public function uninstall() {
		$this->load->model('setting/event');
		$this->load->model('extension/module/multi_admin');
		$this->model_extension_module_multi_admin->uninstall();
	
        $this->model_setting_event->deleteEventByCode('event_category_info');
        $this->model_setting_event->deleteEventByCode('event_category_total_count');
		$this->model_setting_event->deleteEventByCode('event_column_left_modification');
		$this->model_setting_event->deleteEventByCode('event_login_after');
		$this->model_setting_event->deleteEventByCode('event_logout_before');
    }

    public function index() {
        /**
         * Loads the language file. Path of the file along with file name must be given
         */
        $this->load->language('extension/module/multi_admin');
        /**
         * Sets the title to the html page
         */
        $this->document->setTitle($this->language->get('heading_title'));
        /**
         * Loads the model file. Path of the file to be given
         */
        $this->load->model('setting/setting');
        /**
         * Checks whether the request type is post. If yes, then calls the validate function.
         */
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            /**
             * The function named 'editSetting' of a model is called in this way
             * The first argument is the code of the module and the second argument contains all the post values
             * The code must be same as your file name
             */
            $this->model_setting_setting->editSetting('multi_admin', $this->request->post);
            /**
             * The success message is kept in the session
             */
            $this->session->data['success'] = $this->language->get('text_success');
            /**
             * The redirection works in this way.
             * After insertion of data, it will redirect to extension/module file along with the token
             * The success message will be shown there
             */
            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }
        /**
         * Putting the language into the '$data' array
         * This is the way how you get the language from the language file
         */
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_status'] = $this->language->get('entry_status');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        /**
         * If there is any warning in the private property '$error', then it will be put into '$data' array
         */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        /**
         * Breadcrumbs are declared as array
         */
        $data['breadcrumbs'] = array();
        /**
         * Breadcrumbs are defined
         */
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/multi_admin', 'user_token=' . $this->session->data['user_token'], true)
        );
        /**
         * Form action url is created and defined to $data['action']
         */
        $data['action'] = $this->url->link('extension/module/multi_admin', 'user_token=' . $this->session->data['user_token'], true);
        /**
         * Cancel/back button url which will lead you to module list
         */
        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        /**
         * checks whether the value exists in the post request
         */
        if (isset($this->request->post['multi_admin_status'])) {
            $data['multi_admin_status'] = $this->request->post['multi_admin_status'];
        } else {
        /**
         * if the value do not exists in the post request then value is taken from the config i.e. setting table
         */
            $data['multi_admin_status'] = $this->config->get('multi_admin_status');
        }
        /**
         * Header data is loaded
         */
        $data['header'] = $this->load->controller('common/header');
        /**
         * Column left part is loaded
         */
        $data['column_left'] = $this->load->controller('common/column_left');
        /**
         * Footer data is loaded
         */
        $data['footer'] = $this->load->controller('common/footer');
        /**
         * Using this function tpl file is called and all the data of controller is passed through '$data' array
         * This is for Opencart 2.2.0.0 version. There will be minor changes as per the version.
         */
        $this->response->setOutput($this->load->view('extension/module/multi_admin', $data));
    }

    /**
     * validate function validates the values of the post and also the permission
     * @return boolean return true if any of the index of $error contains value
     */
    protected function validate() {
        /**
         * Check whether the current user has the permissions to modify the settings of the module
         * The permissions are set in System->Users->User groups
         */
        if (!$this->user->hasPermission('modify', 'extension/module/multi_admin')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
    }

    public function list(){
        $this->load->language('extension/module/multi_admin');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module/multi_admin');

		$this->getList();
    }

    public function add() {
		$this->load->language('extension/module/multi_admin');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module/multi_admin');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
        
			$this->model_extension_module_multi_admin->addMultiAdmin($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/multi_admin/list', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('extension/module/multi_admin');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module/multi_admin');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_extension_module_multi_admin->editMultiAdmin($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/multi_admin/list', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('extension/module/multi_admin');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module/multi_admin');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_extension_module_multi_admin->deleteMultiAdmin($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/multi_admin/list', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

    protected function getList() {
	
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/multi_admin/list', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('extension/module/multi_admin/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('extension/module/multi_admin/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['users'] = array();

		$filter_data = array(
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

        $user_total = $this->model_extension_module_multi_admin->getTotalMultiAdmin();

        $results = $this->model_extension_module_multi_admin->getMultiAdmins($filter_data);

		foreach ($results as $result) {
			$data['users'][] = array(
				'id'    => $result['id'],
                'username'   => $result['username'],
                'role_name'   => $result['user_group_name'],
                'store_name'   => $result['store_name'],
				//'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				//'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'edit'       => $this->url->link('extension/module/multi_admin/edit', 'user_token=' . $this->session->data['user_token'] . '&id=' . $result['id'] . $url, true)
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url = '';

		$pagination = new Pagination();
		$pagination->total = $user_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('user/user', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($user_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($user_total - $this->config->get('config_limit_admin'))) ? $user_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $user_total, ceil($user_total / $this->config->get('config_limit_admin')));

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/multi_admin_list', $data));
    }
    
    protected function getForm() {
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['username'])) {
			$data['error_username'] = $this->error['username'];
		} else {
			$data['error_username'] = '';
		}

		if (isset($this->error['role'])) {
			$data['error_role_name'] = $this->error['role'];
		} else {
			$data['error_role_name'] = '';
		}

		if (isset($this->error['store'])) {
			$data['error_store_name'] = $this->error['store'];
		} else {
			$data['error_store_name'] = '';
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/multi_admin/list', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('extension/module/multi_admin/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('extension/module/multi_admin/edit', 'user_token=' . $this->session->data['user_token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('extension/module/multi_admin/list', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$user_info = $this->model_extension_module_multi_admin->getMultiAdmin($this->request->get['id']);
		}

		if (isset($this->request->post['user_id'])) {
			$data['user_id'] = $this->request->post['user_id'];
		} elseif (!empty($user_info)) {
			$data['user_id'] = $user_info['user_id'];
		} else {
			$data['user_id'] = '';
		}

		if (isset($this->request->post['user_group_id'])) {
			$data['user_group_id'] = $this->request->post['user_group_id'];
		} elseif (!empty($user_info)) {
			$data['user_group_id'] = $user_info['user_group_id'];
		} else {
			$data['user_group_id'] = '';
        }
        
        if (isset($this->request->post['store_id'])) {
			$data['store_id'] = $this->request->post['store_id'];
		} elseif (!empty($user_info)) {
			$data['store_id'] = $user_info['store_id'];
		} else {
			$data['store_id'] = '';
		}

		$this->load->model('user/user');

        $data['users'] = $this->model_user_user->getUsers();
        
        // $this->load->model('extension/module/multi_admin');

        // $data['roles'] = $this->model_extension_module_multi_admin->getRoles();
  
        
        $this->load->model('setting/store');

		$data['stores'] = $this->model_setting_store->getStores();
		
		$this->load->model('user/user_group');

		$data['user_groups'] = $this->model_user_user_group->getUserGroups();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/multi_admin_form', $data));
    }
    
    protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'extension/module/multi_admin')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$user_info = $this->model_extension_module_multi_admin->getMultiAdminByUseridStoreid($this->request->post['user_id'], $this->request->post['store_id']);
		
		if (!isset($this->request->get['id'])) {
			if ($user_info) {
				$this->error['warning'] = $this->language->get('error_exist_assign_role');
			}
		} else {
			if ($user_info) {
				$this->error['warning'] = $this->language->get('error_exist_assign_role');
			}
		}

		if (!isset($this->request->post['user_id'])) {
			$this->error['username'] = $this->language->get('error_username');
        } 
        
        if (!isset($this->request->post['user_group_id'])) {
			$this->error['user_group_id'] = $this->language->get('error_role_name');
        } 
        
        if (!isset($this->request->post['store_id'])) {
			$this->error['store'] = $this->language->get('error_store_name');
		} 

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'extension/module/multi_admin')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

}