<?php
// catalog/controller/custom/product.php
class ControllerCustomCategory extends Controller {
    public function info($route, $filter_data) {
		$this->load->model('custom/category');

		$url = '';
		// if(isset($this->session->data['store_id']) && !empty($this->session->data['store_id'])){
		// 	$filter_data['store_id'] = $this->session->data['store_id'];
		// }
		
		$filter_data['store_id'] = $this->config->get('config_store_id');
		
		
		$results = $this->model_custom_category->getProductLastModifiedInfo($filter_data);

		foreach ($results as $result) {
			$data['categories'][] = array(
				'category_id' => $result['category_id'],
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
				'edit'        => $this->url->link('catalog/category/edit', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $result['category_id'] . $url, true),
				'delete'      => $this->url->link('catalog/category/delete', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $result['category_id'] . $url, true)
			);
		}

		return $results;
    }

    public function totalcountcategory(){
		$this->load->model('custom/category');
		$store_id = '';
		
		$store_id = $this->config->get('config_store_id');
		
        return $category_total = $this->model_custom_category->getTotalCategories($store_id);
	}

	public function columnleftmodification(&$route = false, &$data = false, &$output = false){

		$this->load->language('extension/module/multi_admin');

		$data['menus'][7]['children'][] = array(
            'name'     => $this->language->get('text_multi_admin'),
            'href'     => $this->url->link('extension/module/multi_admin/list', 'user_token=' . $this->session->data['user_token'], true),
            'children' => array()
        );
	}

	public function loginafter(&$route, &$data = false, &$output=false){
		

		// echo 'hello';die;
		// echo $this->session->data['user_id'];
		// echo '<pre>';
		// print_r($data);die;
		// echo $this->config->get('store_id');die;
		// echo '<pre>';
		// print_r($data);
		// $this->error['warning'] = $this->language->get('error_permission');
		// $this->session->data['attempt'] = false;
		// echo 'hello';die;
		
		if(!isset($this->session->data['store_id'])){
			$username = '';
			if(isset($this->request->post['username'])){
				$username = $this->request->post['username'];
			}
			
			// $password = $this->request->post['username'];
			if($this->config->get('config_store_id') > 0){
				// echo 'hello';die;
				// $user_id = $this->session->data['user_id'];
				$user_group_id = 11;
				// echo $user_group_id = $this->user->getGroupId();
				// $user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
				
				$user_query = $this->db->query("SELECT a.*, b.* FROM " . DB_PREFIX . "user_group_store a JOIN " . DB_PREFIX . "user b ON (a.user_id = b.user_id) WHERE a.store_id = '" . $this->config->get('config_store_id') . "' AND b.username = '" . $this->db->escape($username) . "'");
				// echo '<pre>';
				// print_r($user_query);die;
				echo $this->config->get('config_store_id');
	// echo 'hello';
				if ($user_query->num_rows) {
					
					// if($user_query->row['user_id'] != $this->session->data['user_id']){
					// 	// echo 'hello';die;
					// 	exit('Username or password incorrect make sure you are store admin!');
					// }
					
					$this->session->data['store_id'] = $user_query->row['store_id'];

					// $this->user_id = $user_query->row['user_id'];
					// $this->username = $user_query->row['username'];
					// $this->user_group_id = $user_query->row['user_group_id'];

					// $user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

					// $permissions = json_decode($user_group_query->row['permission'], true);

					// if (is_array($permissions)) {
					// 	foreach ($permissions as $key => $value) {
					// 		$this->permission[$key] = $value;
					// 	}
					// }

					// return true;
					// $this->session->data['store_admin'] = true;
				}
				else{
					unset($this->request->post['username']);
				// 				echo '<pre>';
				// print_r($this->request->post['username']);die;
					// echo $this->user->isLogged();die;
					// echo 'hello123';die;
					$data['error_warning'] = 'There is no store admin for this store!';
					
					// $this->session->data['store_admin'] = false;
				}
			}
		}
	}

	public function logoutbefore($route){
		unset($this->session->data['store_id']);
	}

}